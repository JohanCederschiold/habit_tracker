package se.yrgo.habitizer;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.assertj.core.util.Arrays;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import se.yrgo.habitizer.data.ActivityRepo;
import se.yrgo.habitizer.model.Activity;
import se.yrgo.habitizer.model.ActivityDTO;
import se.yrgo.habitizer.services.ActivityService;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ActivityControllerTest {

	@LocalServerPort
	int port;

	@Autowired
	ActivityService activity;

	@Autowired
	TestRestTemplate template;

	@Autowired
	private ActivityRepo activityRepo;

	@Test
	public void testGetActivityList() {
		fillDatabase();
		assertTrue(activity.getAllActivities() != null);
	}

	@Test
	public void findActivity() {
		fillDatabase();
		assertTrue(this.activity.findActivity(1L) != null);
	}

	public void fillDatabase() {
		ActivityDTO activityDTO1 = new ActivityDTO(LocalDateTime.of(2018, 1, 1, 12, 30), true);
		ActivityDTO activityDTO2 = new ActivityDTO(LocalDateTime.of(2018, 6, 1, 16, 00), true);
		ActivityDTO activityDTO3 = new ActivityDTO(LocalDateTime.of(2019, 2, 2, 15, 35), true);
		ActivityDTO activityDTO4 = new ActivityDTO(LocalDateTime.of(2019, 9, 16, 9, 30), true);

		activity.saveActivity(activityDTO1);
		activity.saveActivity(activityDTO2);
		activity.saveActivity(activityDTO3);
		activity.saveActivity(activityDTO4);

	}
	@Test
	public void registerUser() {
		ActivityDTO activityRegister = new ActivityDTO(LocalDateTime.now(), true);
		registerUserREST(activityRegister);
		assertTrue(this.activity.getAllActivities() != null);
	}
	
//	@Test
	public void testAllGetUsers() {
		fillDatabase();
		assertTrue(activityRepo.findAll() != null);
	}

	
	public void registerUserREST(ActivityDTO activity) {
		ObjectMapper mapper = new ObjectMapper();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = null;
		String payload = null;
		try {
			payload = mapper.writeValueAsString(activity);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		entity = new HttpEntity<String>(payload, headers);
		this.template.postForObject("http://localhost:" + port + "/saveActivity", entity, Activity.class);
	}
}
