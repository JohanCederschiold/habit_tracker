package se.yrgo.habitizer;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import se.yrgo.habitizer.model.User;
import se.yrgo.habitizer.model.UserDTO;
import se.yrgo.habitizer.services.UserService;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class UserControllerTest {

	@LocalServerPort
	private int port;
	
	@Autowired
	TestRestTemplate template;
	
	@Autowired
	UserService service;
	
	@AfterEach
	private void deleteData() {
		List<UserDTO> dbContent = service.getAllUsers();
		for (UserDTO user : dbContent) {
			service.deleteUser(user.getId());
			System.out.println("Deleted User With id " + user.getUserName());
		}
	}
	
	@Test
	public void persistUserViaREST() {
		
//		Create a User to persist. 
		final String USER_NAME = "Mista User";
		User user = new User();
		user.setUserName(USER_NAME);
		
		storeUsersViaRest(user);

		UserDTO foundUser = service.getUserByUserName(USER_NAME);
		
		assertThat(USER_NAME.equals(foundUser.getUserName()));
		
	}
	
	@Test
	public void RegisterThreeUsersAndCheckThatTheCorrectNumberIsStored() {
		
		String [] USER_NAMES = {"Darth Vader", "Qui Gon", "Greedo"};
		
		List<User> users = new ArrayList<User>();
		
		for (String name : USER_NAMES) {
			User user = new User();
			user.setUserName(name);
			users.add(user);
		}
		
		storeUsersViaRest(users);
		
		
		List<UserDTO> response = service.getAllUsers();
		
		assertThat(response).hasSize(USER_NAMES.length);
		
	}	
	
	@Test
	public void testStoringAndDeletingUsersAndCheckingFinalSum() {
		String [] FIRST_USERS = {"Darth Vader", "Qui Gon", "Greedo", "Obi-Wan Kenobi", "Admiral Ackbar"};
		String [] SECOND_USERS = {"Han Solo", "Yoda", "Princess Amidala", "Darth Maul", "Senator Palpatine"};
		
		List<User> users = new ArrayList<User>();
		
		for (String name : FIRST_USERS) {
			User user = new User();
			user.setUserName(name);
			users.add(user);
		}
		
		storeUsersViaRest(users);
		
//		Delete three users from first group via rest. Find them via username, check id and delete.
		String [] deleteUs = {"Darth Vader", "Greedo", "Obi-Wan Kenobi" };
		List<Long> deleteIds = new ArrayList<Long>();
		for (String deleteUser : deleteUs) {
			deleteIds.add(service.getUserByUserName(deleteUser).getId());
		}
		
		

		for (Long id : deleteIds) {
			this.template.delete("http://localhost:" + port + "/deleteuser/" + id);
		}
		
		users = new ArrayList<User>();
		
//		Add five more users and make sure that final count is correct. 
		for (String name : SECOND_USERS) {
			User user = new User();
			user.setUserName(name);
			users.add(user);
		}
		
		storeUsersViaRest(users);
		
		List<UserDTO> response = service.getAllUsers();

		assertThat(response).hasSize(7);

	}
	
	
	@Test
	public void storeUserAndChangeIt() {
		
		String FIRST_USER_NAME = "User McUserson";
		String SECOND_USER_NAME = "User Usabon";
		
//		Create and store first user
		User firstUser = new User();
		firstUser.setUserName(FIRST_USER_NAME);
		
		storeUsersViaRest(firstUser);
		
//		Retrieve user
		UserDTO retrieved = this.template.getForObject("http://localhost:" + port + "/username/" + FIRST_USER_NAME, UserDTO.class);
		Long id = retrieved.getId();
		
//		Change user
		User changedUser = new User();
		changedUser.setUserName(SECOND_USER_NAME);
		changedUser.setId(id);
		
//		Updateuser
		updateUsersViaRest(changedUser);
		
		List<UserDTO> response = service.getAllUsers();
		assertThat(response).hasSize(1);
		assertThat(response.get(0).getUserName().contentEquals(SECOND_USER_NAME));
		
		
	}
	
	
	
	
//	Support methods
	
	private void storeUsersViaRest (List<User> users) {
		ObjectMapper mapper = new ObjectMapper();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = null;
		
//		Iterate over users and register them via REST. 
		for (User createdUser : users ) {
			String payload = null;
			try {
				payload = mapper.writeValueAsString(createdUser);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
			
			entity = new HttpEntity<String>(payload, headers);
			
			this.template.postForObject("http://localhost:" + port + "/register", 
					entity, User.class);
		}
	}
	
	private void storeUsersViaRest(User user) {
		List<User> users = new  ArrayList<>(Arrays.asList(user));
		storeUsersViaRest(users);
	}
	
	private void updateUsersViaRest (List<User> users) {
		ObjectMapper mapper = new ObjectMapper();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = null;
		
//		Iterate over users and register them via REST. 
		for (User userToUpdate : users ) {
			String payload = null;
			try {
				payload = mapper.writeValueAsString(userToUpdate);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
			
			entity = new HttpEntity<String>(payload, headers);
			
			this.template.postForObject("http://localhost:" + port + "/updateuser", 
					entity, User.class);
		}
	}
	
	private void updateUsersViaRest(User user) {
		updateUsersViaRest(new ArrayList<User>(Arrays.asList(user)));		
	}
 }
