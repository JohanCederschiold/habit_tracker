package se.yrgo.habitizer.repo;

import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.junit5.DBUnitExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import se.yrgo.habitizer.controllers.HabitController;
import se.yrgo.habitizer.data.HabitRepo;
import se.yrgo.habitizer.model.HabitDTO;
import se.yrgo.habitizer.services.HabitService;

@ExtendWith({DBUnitExtension.class, SpringExtension.class})
// Makes adding DataSet("something.yaml") to the test classes to make them use a preset dataset.
// The preset dataset should be added in test/resources/datasets/someclass.yml
// DBUnitExtension manages the database context and adds and removes data automatically from the configured test database during testing.
// Then we will always have a known set of data to work with during testing.
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class HabitRepoUnitTest {

//TODO: Unit testing for the repo class.

}
