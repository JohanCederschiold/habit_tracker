package se.yrgo.habitizer.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.hamcrest.CoreMatchers.is;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import se.yrgo.habitizer.data.UserRepo;
import se.yrgo.habitizer.model.HabitDTO;
import se.yrgo.habitizer.services.ActivityService;
import se.yrgo.habitizer.services.HabitService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(HabitController.class)
@AutoConfigureMockMvc
public class HabitControllerUnitTest {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    HabitService service;
    @MockBean
    ActivityService sls;
    @MockBean
    UserRepo userRepos;

    private LocalDateTime startDate = LocalDateTime.now();
    private LocalDateTime stopDate = LocalDateTime.now().plusDays(7);

    private HabitDTO mockCreateHabitDTO = new HabitDTO(0L, "My new running habit", startDate, stopDate, 10);
    private HabitDTO mockOneHabitDTO = new HabitDTO(1L, "My new cleaning habit", startDate, stopDate, 10);

    @Test
    @DisplayName("Test POST /createhabit, creates a new Habit")
    void testCreateANewHabit() throws Exception {

        doReturn(mockCreateHabitDTO).when(service).registerHabit(any());

        mockMvc.perform(
                post("/createhabit")
                        .content(asJsonString(mockCreateHabitDTO))
                        .contentType(MediaType.APPLICATION_JSON))

                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.title").isString())
                .andExpect(jsonPath("$.title", is("My new running habit")));

    }

    @Test
    @DisplayName("Test GET /habits, get all habits")
    void testGetAllHabits() throws Exception {

        List<HabitDTO> mockList = new ArrayList<>();
        doReturn(mockList).when(service).getAllHabits();

        mockMvc.perform(
                get("/habits"))
                .andExpect(status().isOk());

    }

    @Test
    @DisplayName("Test GET /habit/{id}, get one habit by id")
    void testGetOneHabitById() throws Exception {

        doReturn(mockOneHabitDTO).when(service).getHabitDTOById(1L);

        mockMvc.perform(get("/habit/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)));

    }

    @Test
    @DisplayName("Test DELETE /deletehabit/{id}, delete one habit by id")
    void testDeletingHabitById() throws Exception {

        doNothing().when(service).deleteHabitById(1L);

        mockMvc.perform(delete("/deletehabit/{id}", 1))
                .andExpect(status().isAccepted());

    }

    @Test
    @DisplayName("Test PUT /updatehabit, update one habit from requestbody habitDTO json object")
    void testUpdateOneHabit() throws Exception {

        doReturn(mockOneHabitDTO).when(service).updateHabit(any());

        mockMvc.perform(put("/updatehabit")
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.IF_MATCH, 1)
                .content(asJsonString(mockOneHabitDTO)))

                .andExpect(status().isAccepted())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));

    }

    static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}