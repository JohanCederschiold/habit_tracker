package se.yrgo.habitizer.service;

import static org.junit.jupiter.api.Assertions.*;

import com.sun.xml.bind.v2.TODO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import se.yrgo.habitizer.data.HabitRepo;
import se.yrgo.habitizer.model.Activity;
import se.yrgo.habitizer.model.Habit;
import se.yrgo.habitizer.model.HabitDTO;
import se.yrgo.habitizer.services.ActivityService;
import se.yrgo.habitizer.services.HabitServiceImpl;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class HabitServiceUnitTest {

    @Autowired
    private HabitServiceImpl service;

    @MockBean
    private HabitRepo habitRepo;

    private LocalDateTime startDate = LocalDateTime.now();
    private LocalDateTime stopDate = LocalDateTime.now().plusDays(7);

    private HabitDTO mockHabitDTO = new HabitDTO(0L, "My new running habit", startDate, stopDate, 10);
    private HabitDTO mockHabitDTOTwo = new HabitDTO(1L, "My new running habit", startDate, stopDate, 10);
    private HabitDTO mockHabitDTOThree = new HabitDTO(2L, "My new cleaning habit", startDate, stopDate, 20);

    @Test
    void testRegisterHabit() {

        doReturn(service.convertFromDTO(mockHabitDTO)).when(habitRepo).save(any());

        HabitDTO returnedHabitDTO = service.registerHabit(mockHabitDTO);

        assertNotNull(returnedHabitDTO, "The returned object should not be null");
        assertEquals("My new running habit",
                                returnedHabitDTO.getTitle(),
                                "The title value in the returned object should match the registered object's title");
    }

    @Test
    void testGetAllHabits() {

        doReturn(Arrays.asList(service.convertFromDTO(mockHabitDTOTwo),
                                service.convertFromDTO(mockHabitDTOThree)))
                                .when(habitRepo).findAll();

        List<HabitDTO> products = service.getAllHabits();

        assertEquals(2, products.size(), "getAllHabits should return 2 habits");
    }

    @Test
    void testGetOneHabitByID() {

        Habit habit = service.convertFromDTO(mockHabitDTOTwo);
        doReturn(habit).when(habitRepo).getOne(2L);

        HabitDTO habitDTO = service.getHabitDTOById(2L);

        assertNotNull(habitDTO);
        assertEquals("My new running habit", habitDTO.getTitle());

    }

    @Test
    void testDeleteOneHabit() {

        service.deleteHabitById(2L);
        verify(habitRepo, times(1)).deleteById(2L);

    }

//    TODO: Add more unit tests for the Habit Service class
//    @Test
//    void testUpdateHabit() {
//
////        Habit habit = service.convertFromDTO(mockHabitDTOThree);
//        doReturn(mockHabitDTOThree).when(habitRepo).getOne(mockHabitDTOThree.getId());
//
//        HabitDTO updatedHabitDTO = service.updateHabit(mockHabitDTOThree);
//
//        assertNotNull(updatedHabitDTO);
//        assertEquals(20, updatedHabitDTO.getGoal(), "The updated habit-goal should be 20");
//
//    }

}
