package se.yrgo.habitizer.data;

import org.springframework.data.jpa.repository.JpaRepository;

import se.yrgo.habitizer.model.Habit;

public interface HabitRepo extends JpaRepository<Habit, Long> {

}
