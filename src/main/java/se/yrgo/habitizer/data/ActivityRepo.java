package se.yrgo.habitizer.data;

import org.springframework.data.jpa.repository.JpaRepository;

import se.yrgo.habitizer.model.Activity;

public interface ActivityRepo extends JpaRepository<Activity, Long> {

}
