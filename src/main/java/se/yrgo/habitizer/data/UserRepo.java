package se.yrgo.habitizer.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import se.yrgo.habitizer.model.User;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {
	
	public User findUserByUserNameIgnoreCase(String userName);

}
