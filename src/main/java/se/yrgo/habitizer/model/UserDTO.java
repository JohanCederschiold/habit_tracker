package se.yrgo.habitizer.model;

public class UserDTO {
	
	private String userName;
	private Long id;

	
	public UserDTO(String userName, Long id) {
		super();
		this.userName = userName;
		this.id = id;
	}



	public UserDTO() {
		super();
	}



	public String getUserName() {
		return userName;
	}
	
	public Long getId() {
		return this.id;
	}
	

	public void setId(Long id) {
		this.id = id;
	}



	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String toString() {
		return "UserDTO [userName=" + userName + "]";
	}

}

