package se.yrgo.habitizer.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.*;

@Entity
public class Habit {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String title;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm")
	private LocalDateTime startDate;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm")
	private LocalDateTime stopDate;

	private int goal;

	@OneToMany
	@JoinColumn(name="habit_fk")
	private Set<Activity> activities;

	public Habit() {}

	public Habit(String title, LocalDateTime startDate, LocalDateTime stopDate, int goal) {
		this.title = title;
		this.startDate = startDate;
		this.stopDate = stopDate;
		this.goal = goal;
		this.activities = new HashSet<Activity>();
	}

	public Long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	public LocalDateTime getStopDate() {
		return stopDate;
	}

	public void setStopDate(LocalDateTime stopDate) {
		this.stopDate = stopDate;
	}

	public int getGoal() {
		return goal;
	}

	public void setGoal(int goal) {
		this.goal = goal;
	}

	public Set<Activity> getActivities() {
		Set<Activity> unmodifiable = Collections.unmodifiableSet(this.activities);
		return unmodifiable;
	}

	public void addActivityToHabit(Activity activity) {
		activities.add(activity);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Habit habit = (Habit) o;
		return goal == habit.goal &&
				Objects.equals(id, habit.id) &&
				Objects.equals(title, habit.title) &&
				Objects.equals(startDate, habit.startDate) &&
				Objects.equals(stopDate, habit.stopDate) &&
				Objects.equals(activities, habit.activities);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, title, startDate, stopDate, goal, activities);
	}

	@Override
	public String toString() {
		return "Activity [id=" + id + ", title=" + title + "]";
	}

}
