package se.yrgo.habitizer.model;

import java.time.LocalDateTime;

public class ActivityDTO {
	
		
	private LocalDateTime timeStamp;
	private boolean performed;
	
	public ActivityDTO(LocalDateTime timeStamp, boolean performed) {
		super();
		this.timeStamp = timeStamp;
		this.performed = performed;
	}
	
	public LocalDateTime getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(LocalDateTime timeStamp) {
		this.timeStamp = timeStamp;
	}
	public boolean isPerformed() {
		return performed;
	}
	public void setPerformed(boolean performed) {
		this.performed = performed;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (performed ? 1231 : 1237);
		result = prime * result + ((timeStamp == null) ? 0 : timeStamp.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ActivityDTO other = (ActivityDTO) obj;
		if (performed != other.performed)
			return false;
		if (timeStamp == null) {
			if (other.timeStamp != null)
				return false;
		} else if (!timeStamp.equals(other.timeStamp))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ActivityDTO [timeStamp=" + timeStamp + ", performed=" + performed + "]";
	}
	
	
	
	

}
