	package se.yrgo.habitizer.model;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "habit_user")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="Habit_user", unique = true, nullable = false)
	private String userName;
	
	@OneToMany
	@JoinColumn(name = "user_fk")
	private Set<Habit>habits;

	public User() {
		this(null);
	}
	
	
	public User(String userName) {
		this.userName = userName;
		instantiateSets();
	}


	private void instantiateSets () {
		habits = new HashSet<Habit>();
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public Long getId() {
		return this.id;
	}
	
	
	

	public void setId(Long id) {
		this.id = id;
	}


	public Set<Habit> getHabits() {
		Set<Habit>cantChangeThis = Collections.unmodifiableSet(this.habits);
		return cantChangeThis;
	}


	public void addHabit(Habit habit) {
		this.habits.add(habit);
	}
	
	public void removeHabit(Habit habit) {
		this.habits.remove(habit);
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", userName=" + userName + "]";
	}
	
	

	private String role = "USER";
	private String password = "password";
	
	public void setRole(String role) {
		this.role = role;
	}
	
	public String getRole() {
		return this.role;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getPassword() {
		return this.password;
	} 
	

}

