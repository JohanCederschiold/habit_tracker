package se.yrgo.habitizer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication   
public class HAbitiz3rApplication {

	public static void main(String[] args) {
		SpringApplication.run(HAbitiz3rApplication.class, args);
	}

}
