package se.yrgo.habitizer.controllers;

import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import se.yrgo.habitizer.model.Activity;
import se.yrgo.habitizer.model.ActivityDTO;
import se.yrgo.habitizer.services.ActivityService;
import se.yrgo.habitizer.wrappers.ActivityWrapper;

@RestController
public class ActivityController {

	@Autowired
	public ActivityService activityService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ActivityController.class);

	
	@PostMapping("/createActivity")
	public Activity createActivit(@RequestBody ActivityDTO activity){
		LOGGER.debug("Activity " + activity + " was saved");
		return activityService.saveActivity(activity);
	}
	
	@GetMapping("/gettimestamp")
	public LocalDateTime getTimeStamp() {
		return activityService.getCurrentTimeStamp();
	}
	
	@PostMapping("/getActivityByID")	
	public ActivityDTO getActivityByID (@RequestBody ActivityWrapper activity){
		LOGGER.debug("Search activity by ID: " + activity.getId());
		return activityService.findActivity(activity.getId());
	}
	
	
	@PostMapping("/getActivitesIntervall")
	public List<ActivityDTO> getActivityIntervall (@RequestBody ActivityWrapper dates) {
		return activityService.findActivityWithinTimeRange(dates.getFromDate().toLocalDate(), dates.getFromTime(), dates.getToDate().toLocalDate(), dates.getFromTime());
	}
	
	@GetMapping("/getallactivites")
	public List<ActivityDTO> getAllActivites() {
		List<ActivityDTO> activityList = activityService.getAllActivities();
		LOGGER.debug(activityList.size() + " activites was found in the database and returned");
		return activityService.getAllActivities();
	}
	
	
}
