package se.yrgo.habitizer.controllers;

public class HabitActivityConnectionException extends RuntimeException {

    public HabitActivityConnectionException(String message) {
        super(message);
    }

}
