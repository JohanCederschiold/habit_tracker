package se.yrgo.habitizer.controllers;

import se.yrgo.habitizer.model.Habit;

public class HabitNotFoundException extends RuntimeException {

    public HabitNotFoundException(String message) {
        super(message);
    }

    public HabitNotFoundException(Long id) {
        super("Habit with the id " + id + " does not exist.");
    }
}
