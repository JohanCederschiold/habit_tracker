package se.yrgo.habitizer.controllers;

import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.yrgo.habitizer.model.Activity;
import se.yrgo.habitizer.model.HabitDTO;
import se.yrgo.habitizer.services.ActivityService;
import se.yrgo.habitizer.services.HabitService;
import se.yrgo.habitizer.wrappers.HabitAndActivityWrapper;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.NoSuchElementException;

@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Successfully retrieved list"),
		@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
		@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
		@ApiResponse(code = 404, message = "The resource you were trying to reach were not found")
})
@RestController
public class HabitController {

	private static final Logger LOGGER = LoggerFactory.getLogger(HabitController.class);

	private final String CREATE_HABIT_DESCRIPTION = "Register a new habit.";
	private final String HABIT_REQUESTBODY_DESCRIPTION = "Habit object to store in database table.";
	private final String DELETE_HABIT_DESCRIPTION = "Delete one habit by id.";
	private final String GET_ALL_HABITS_DESCRIPTION = "Returns a list of all the habits in the database.";
	private final String GET_ONE_HABIT_DESCRIPTION = "Returns the habit object that matches the id path variable.";
	private final String UPDATE_HABIT_DESCRIPTION = "Updates one habit that matches the id in the request body object.";
	private final String ADD_ACTIVITY_TO_HABIT = "Connects an activity to a habit";
	private final String GET_ALL_ACTIVITIES_FOR_ONE_HABIT_BY_ID = "Return all the activities a Habit, specified by ID, is connected to.";

	@Autowired
	private HabitService service;
	@Autowired
	private ActivityService activityService;

	@ApiOperation(value = CREATE_HABIT_DESCRIPTION, response = HabitDTO.class)
	@PostMapping("/createhabit")
	@ResponseStatus(HttpStatus.CREATED)
	public HabitDTO createHabit( @ApiParam(value = HABIT_REQUESTBODY_DESCRIPTION, required = true)
								 @RequestBody final HabitDTO habit) {

		if (habit.getTitle() == null) {
			throw new IncorrectRequestException();
		}
		LOGGER.debug("Creating habit: " + habit.getTitle());
		return service.registerHabit(habit);

	}

	@ApiOperation(value = GET_ALL_HABITS_DESCRIPTION, response = List.class)
	@GetMapping("/habits")
	public List<HabitDTO> getAllHabits() {

		List<HabitDTO> habits = service.getAllHabits();
		LOGGER.debug("GET /habits : Number of habits returned: " + habits.size());
		return habits;

	}

	@ApiOperation(value = GET_ONE_HABIT_DESCRIPTION, response = HabitDTO.class)
	@GetMapping("/habit/{id}")
	public HabitDTO getOneHabit(@PathVariable Long id) {

		try {
			service.getHabitDTOById(id);
		} catch (EntityNotFoundException e) {
			throw new HabitNotFoundException(id);
		}
		LOGGER.debug("GET /habit/" + id);
		return service.getHabitDTOById(id);

	}

	@ApiOperation(value=DELETE_HABIT_DESCRIPTION)
	@DeleteMapping("/deletehabit/{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public ResponseEntity deleteHabit(@PathVariable Long id) {

		try {
			service.getHabitDTOById(id);
		} catch (EntityNotFoundException e) {
			throw new HabitNotFoundException(id);
		}
		LOGGER.debug("DELETE /deletehabit/" + id);
		service.deleteHabitById(id);
		return new ResponseEntity("Habit with id " + id + " deleted", HttpStatus.ACCEPTED);

	}

	@ApiOperation(value = UPDATE_HABIT_DESCRIPTION, response = HabitDTO.class)
	@PutMapping("/updatehabit")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public HabitDTO updateHabit(@RequestBody final HabitDTO habitDTO) {

		try {
			service.getHabitDTOById(habitDTO.getId());
		} catch (EntityNotFoundException e) {
			throw new HabitNotFoundException(habitDTO.getId());
		}
		LOGGER.debug("UPDATE /updatehabit : " + habitDTO.toString());
		return service.updateHabit(habitDTO);

	}

	@ApiOperation(value = ADD_ACTIVITY_TO_HABIT, response = ResponseEntity.class)
	@PostMapping("/addactivitytohabit")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public ResponseEntity addActivityToHabit(@RequestBody HabitAndActivityWrapper wrapper) {

		try {
			service.getHabitDTOById(wrapper.getHabit());
			activityService.findActivity(wrapper.getActivity());
		} catch (EntityNotFoundException e) {
			throw new HabitActivityConnectionException("Habit with id " + wrapper.getHabit() + " was not found");
		} catch (NoSuchElementException e) {
			throw new HabitActivityConnectionException("Activity with id " + wrapper.getActivity() + " was not found");
		}
		LOGGER.debug("POST: /addactivitytohabit, connecting an activity to a habit.");
		service.addActivityToHabit(wrapper.getActivity(), wrapper.getHabit());

		return new ResponseEntity("Activity with id " + wrapper.getActivity() + ", added to Habit with id " + wrapper.getHabit(),
									HttpStatus.ACCEPTED);

	}

	@ApiOperation(value = GET_ALL_ACTIVITIES_FOR_ONE_HABIT_BY_ID)
	@GetMapping("/getactivitiesforonehabit/{id}")
	public Iterable<Activity> getAllActivitiesForOneHabitById(@PathVariable Long id) {

		try {
			service.getHabitDTOById(id);
		} catch (EntityNotFoundException e) {
			throw new HabitNotFoundException(id);
		}
		return service.getAllActivitiesRelatedToOneHabitByHabitId(id);

	}

}