package se.yrgo.habitizer.controllers;

public class UserNotFoundException extends RuntimeException {
	
	public UserNotFoundException(Long id) {
		super(String.format("I cannot find a user with Id %d", id));
	}
	
	public UserNotFoundException(String name) {
		super(String.format("I cannot find a user with name %s", name));
	}

}
