package se.yrgo.habitizer.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import se.yrgo.habitizer.model.UserDTO;
import se.yrgo.habitizer.security.UserPrincipal;
import se.yrgo.habitizer.services.UserService;
import se.yrgo.habitizer.wrappers.UserAndHabitWrapper;

@RestController
public class UserController {
	
	/*
	Example of how to register a user. 
	{
		"userName": "Usain Bolt"
	}
	
	How to update a user:
	{
		"userName": "Sauron",
		"id": 1
	}
	 *
	 */
	
//	Also see alternative to wire in UserServiceImpl where constructor injection is used (same result).
	@Autowired
	private UserService service;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	
	@PostMapping("/register")
	@ResponseStatus(HttpStatus.CREATED)
	public UserDTO registerUser(@RequestBody UserDTO user) {
		if ( user.getUserName() == null ) {
			throw new IncorrectRequestException();
		}
		LOGGER.debug(String.format("User with Username: %s created\n", user.getUserName()));
		return service.registerUser(user);
	}
	
	@GetMapping("/user/{id}")
	public UserDTO getUserById(@PathVariable Long id) {
		UserDTO foundUser = service.getUserById(id);
		return foundUser;
	}
	
	@GetMapping("/users")
	public List<UserDTO> getAllUsers() {
		return service.getAllUsers();
	}
	
	@GetMapping("/username/{name}")
	public UserDTO getUserByUsername(@PathVariable String name) {
		UserDTO foundUser = service.getUserByUserName(name);
		return foundUser;
	}
	
	@DeleteMapping("/deleteuser/{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void deleteUserById(@PathVariable Long id) {
//		TODO: Should confirm deletion before logging. 
		LOGGER.debug(String.format("User with id %d deleted\n", id));
		service.deleteUser(id);
	}
	
	@PutMapping("/updateuser")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public UserDTO updateUser (@RequestBody UserDTO userToUpdate) {
		if (userToUpdate.getUserName() == null) {
			throw new IncorrectRequestException();
		}
		LOGGER.debug(String.format("User with id %d changed username to %s.\n", userToUpdate.getId(),
				userToUpdate.getUserName()));
		return service.updateUser(userToUpdate);
	}
	
	@PutMapping("/addhabittouser") 
	public void  addHabit (@RequestBody UserAndHabitWrapper wrapper) {
		if (wrapper.getHabit() == null || wrapper.getUser() == null ) {
			throw new IncorrectRequestException();
		}
		LOGGER.debug(String.format("Habit with id %d added to User with id %d.\n", 
				wrapper.getHabit(), 
				wrapper.getUser()));
		service.addHabitToUser(wrapper.getUser(), wrapper.getHabit());
	}
	
	
}
