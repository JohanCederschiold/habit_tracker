package se.yrgo.habitizer.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class HabitControllerAdvice {

    @ResponseBody
    @ExceptionHandler(HabitNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String habitNotFoundHandler(HabitNotFoundException e) {
        return e.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(HabitActivityConnectionException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String habitOrActivityNotFoundHandler(HabitActivityConnectionException e) {
        return e.getMessage();
    }

}