package se.yrgo.habitizer.controllers;

public class IncorrectRequestException extends RuntimeException {
	
	public IncorrectRequestException() {
		super("Hmm...Your request seems inaccurate. Please refer to documentation");
	}

}
