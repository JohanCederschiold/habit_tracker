package se.yrgo.habitizer.services;

import java.util.List;

import se.yrgo.habitizer.model.User;
import se.yrgo.habitizer.model.UserDTO;

public interface UserService {
	
	public UserDTO registerUser(UserDTO userDTO);
	public UserDTO getUserById(Long id);
	public UserDTO getUserByUserName(String userName);
	public List<UserDTO> getAllUsers();
	public void deleteUser(Long id);
	public UserDTO updateUser(UserDTO UserDTO);
	public void addHabitToUser(Long userId, Long habitId);

}
