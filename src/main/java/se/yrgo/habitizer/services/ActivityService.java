package se.yrgo.habitizer.services;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import org.springframework.stereotype.Service;

import se.yrgo.habitizer.model.Activity;
import se.yrgo.habitizer.model.ActivityDTO;

@Service
public interface ActivityService {

	public void setPerformed(ActivityDTO activityDTO);
	public LocalDateTime getCurrentTimeStamp();
	public Activity saveActivity(ActivityDTO activityDTO);
	public ActivityDTO findActivity(Long id);
	public List<ActivityDTO> findActivityWithinTimeRange(LocalDate fromDate, LocalTime fromTime, LocalDate toDate, LocalTime toTime); 
	public List<ActivityDTO> getAllActivities();
}
