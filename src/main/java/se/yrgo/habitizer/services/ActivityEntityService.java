package se.yrgo.habitizer.services;

import se.yrgo.habitizer.model.Activity;

public abstract class ActivityEntityService {
	protected abstract Activity getActivityByID(Long id);
}
