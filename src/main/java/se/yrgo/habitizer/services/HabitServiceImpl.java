package se.yrgo.habitizer.services;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.yrgo.habitizer.data.ActivityRepo;
import se.yrgo.habitizer.data.HabitRepo;
import se.yrgo.habitizer.model.Activity;
import se.yrgo.habitizer.model.Habit;
import se.yrgo.habitizer.model.HabitDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class HabitServiceImpl extends HabitEntityService implements HabitService {

	@Autowired
	private HabitRepo habitRepo;

	@Autowired
	private ActivityEntityService activityEntityService;
	
	public HabitServiceImpl() {}

	@Override
	protected Habit getHabitEntityById(Long id) {

		Habit habit = habitRepo.findById(id).get();
		return habit;

	}

	@Override
	public HabitDTO registerHabit(HabitDTO habitDTO) {

		Habit habit = convertFromDTO(habitDTO);
		return convertToDTO(habitRepo.save(habit));

	}

	@Override
	public List<HabitDTO> getAllHabits() {

		List<Habit> habits = habitRepo.findAll();
		List<HabitDTO> habitDTOs = new ArrayList<>();
		for (Habit habit : habits) {
			habitDTOs.add(convertToDTO(habit));
		}
		return habitDTOs;

	}

	@Override
	public HabitDTO getHabitDTOById(Long id) {

		return convertToDTO(habitRepo.getOne(id));

	}

	@Override
	public void deleteHabitById(Long id) {

		habitRepo.deleteById(id);

	}

	@Override
	public void addActivityToHabit(Long activityId, Long habitId) {

		Activity activity = activityEntityService.getActivityByID(activityId);
		Habit habit = habitRepo.findById(habitId).get();
		habit.addActivityToHabit(activity);
		habitRepo.save(habit);

	}

	@Override
	public Set<Activity> getAllActivitiesRelatedToOneHabitByHabitId(Long habitId) {

		Set<Activity> activities = habitRepo.getOne(habitId).getActivities();
		return activities;

	}

	@Override
	public HabitDTO updateHabit(HabitDTO habitDTO) {

		Habit habitToUpdate = habitRepo.getOne(habitDTO.getId());
		BeanUtils.copyProperties(convertFromDTO(habitDTO), habitToUpdate, "id");
		return convertToDTO(habitRepo.save(habitToUpdate));

	}

	public Habit convertFromDTO(HabitDTO dto) {

		Habit habit = new Habit();
		habit.setTitle(dto.getTitle());
		habit.setStartDate(dto.getStartDate());
		habit.setStopDate(dto.getStopDate());
		habit.setGoal(dto.getGoal());

		return habit;

	}

	public HabitDTO convertToDTO(Habit habit) {

		return new HabitDTO(habit.getId(),
							habit.getTitle(),
							habit.getStartDate(),
							habit.getStopDate(),
							habit.getGoal());

	}

}
