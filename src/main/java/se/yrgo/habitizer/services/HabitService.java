package se.yrgo.habitizer.services;

import se.yrgo.habitizer.model.Activity;
import se.yrgo.habitizer.model.Habit;
import se.yrgo.habitizer.model.HabitDTO;
import se.yrgo.habitizer.wrappers.HabitAndActivityWrapper;

import java.util.List;
import java.util.Set;

public interface HabitService {

	List<HabitDTO> getAllHabits();
	HabitDTO registerHabit(HabitDTO habitDTO);
	HabitDTO getHabitDTOById(Long id);
	HabitDTO updateHabit(HabitDTO habitDTO);
	void deleteHabitById(Long id);
	HabitDTO convertToDTO(Habit habit);
	Habit convertFromDTO(HabitDTO dto);
	void addActivityToHabit(Long activityId, Long habitId);
	Set<Activity> getAllActivitiesRelatedToOneHabitByHabitId(Long habitId);

}
