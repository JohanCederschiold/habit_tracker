package se.yrgo.habitizer.services;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.stereotype.Service;

import se.yrgo.habitizer.controllers.IncorrectRequestException;
import se.yrgo.habitizer.controllers.UserNotFoundException;
import se.yrgo.habitizer.data.UserRepo;
import se.yrgo.habitizer.model.Habit;
import se.yrgo.habitizer.model.User;
import se.yrgo.habitizer.model.UserDTO;

@Service
public class UserServiceImpl extends UserEntityService implements UserService  {
	
	private UserRepo repo;
	private HabitEntityService habitRepo;
	
	
//	Constructor-based injection (an alternative to putting @Autowired on property above)
	public UserServiceImpl(UserRepo repo, HabitEntityService habitRepo) {
		this.repo = repo;
		this.habitRepo = habitRepo;
	}

	@Override
	public UserDTO registerUser(UserDTO userDTO) {

		User user = repo.save(convertFromDTO(userDTO));
		return convertToDTO(user);
	}


	@Override
	public UserDTO getUserById(Long id) {
		
		UserDTO foundUser = null;
		try {
			foundUser = convertToDTO(repo.getOne(id));
		} catch (EntityNotFoundException e) {
			throw new UserNotFoundException(id);
		}
		return foundUser;

	}

	@Override
	public UserDTO getUserByUserName(String userName) {
		
		UserDTO foundUser = null;
		try {
			foundUser = convertToDTO(repo.findUserByUserNameIgnoreCase(userName));
		} catch (NullPointerException e) {
			throw new UserNotFoundException(userName);
		}
		return foundUser;		
	}

	@Override
	public List<UserDTO> getAllUsers() {
		
		List<User>allUsers = repo.findAll();
		List<UserDTO> allUserDTOs = new ArrayList<UserDTO>();
		for (User user : allUsers) {
			allUserDTOs.add(convertToDTO(user));
		}
		
		return allUserDTOs;
	}
	
	@Override
	public void deleteUser(Long id) {
		repo.deleteById(id);
	}
	
	@Override
	public UserDTO updateUser(UserDTO userDTO) {
		User userToUpdate = repo.getOne(userDTO.getId());
		userToUpdate.setUserName(userDTO.getUserName());
		repo.save(userToUpdate);
		return convertToDTO(userToUpdate);
	}
	
	@Override
	public void addHabitToUser(Long userId, Long habitId) {

		Habit habitToAdd = habitRepo.getHabitEntityById(habitId);
		User userToGetHabit = repo.getOne(userId);

		userToGetHabit.addHabit(habitToAdd);
		repo.save(userToGetHabit);
	}
	
	
	private User convertFromDTO(UserDTO dto) {
		User user = new User();
		user.setUserName(dto.getUserName());
		return user;
	}
	
	private UserDTO convertToDTO(User user) {
		return new UserDTO(user.getUserName(), user.getId());
	}

	@Override
	protected User getUserEntityById(Long id) {
		return repo.getOne(id);
	}









}
