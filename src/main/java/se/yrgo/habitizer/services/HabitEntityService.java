package se.yrgo.habitizer.services;

import se.yrgo.habitizer.model.Habit;

public abstract class HabitEntityService {

    protected abstract Habit getHabitEntityById(Long id);

}
