package se.yrgo.habitizer.services;

import se.yrgo.habitizer.model.User;

public abstract class UserEntityService {
	
	protected abstract User getUserEntityById(Long id);
}
