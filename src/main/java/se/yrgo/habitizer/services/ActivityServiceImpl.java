package se.yrgo.habitizer.services;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.yrgo.habitizer.data.ActivityRepo;
import se.yrgo.habitizer.model.Activity;
import se.yrgo.habitizer.model.ActivityDTO;

@Service
public class ActivityServiceImpl extends ActivityEntityService implements ActivityService {

	@Autowired
	private ActivityRepo activityRepo;

	@Override
	public void setPerformed(ActivityDTO activityDTO) {
		Activity activity = convertToActivity(activityDTO);
		activity.setPerformed(true);
		activityRepo.save(activity);
	}

	@Override
	public LocalDateTime getCurrentTimeStamp() {
		LocalDateTime localDateTime = LocalDateTime.now();
		return localDateTime;
	}

	@Override
	public Activity saveActivity(ActivityDTO activityDTO) {
		Activity activity = convertToActivity(activityDTO);
		activityRepo.save(activity);
		return activity;
	}
	
	public ActivityDTO findActivity(Activity actvity) {
		
		return null;
	}

	@Override
	public List<ActivityDTO> getAllActivities() {
		List<ActivityDTO> listActivityDTO = convertListToDTO(activityRepo.findAll());
		return listActivityDTO;
	}

	@Override
	public ActivityDTO findActivity(Long id) {
		Activity activity = activityRepo.findById(id).get();
		ActivityDTO dto = convertToDTO(activity);
		return dto;
	}

//	TODO the select in SQL instead
	@Override
	public List<ActivityDTO> findActivityWithinTimeRange(LocalDate fromDate, LocalTime fromTime, LocalDate toDate,
			LocalTime toTime) {
		List<LocalDateTime> localDateTimeList = CreateLocalDateTime(fromDate, fromTime, toDate, toTime);
		List<Activity> activityList = activityRepo.findAll();
		List<ActivityDTO> newList = new ArrayList<ActivityDTO>();
		for (Activity activity : activityList) {
			if (activity.getTimeStamp().isAfter(localDateTimeList.get(0))
					&& activity.getTimeStamp().isBefore(localDateTimeList.get(1))) {
				newList.add(convertToDTO(activity));
			}
		}
		return newList;
	}

	private List<LocalDateTime> CreateLocalDateTime(LocalDate fromDate, LocalTime fromTime, LocalDate toDate,
			LocalTime toTime) {
		List<LocalDateTime> localDateTimeList = new ArrayList<LocalDateTime>();
		if (fromTime == null && toTime == null) {
			fromTime = LocalTime.of(0, 0);
			toTime = LocalTime.of(0, 0);
		}
		LocalDateTime toDateTime = LocalDateTime.of(toDate, toTime);
		LocalDateTime fromDateTime = LocalDateTime.of(fromDate, fromTime);
		localDateTimeList.add(fromDateTime);
		localDateTimeList.add(toDateTime);
		return localDateTimeList;

	}

	protected ActivityDTO getActivityWithId(Long id) {
		ActivityDTO activityDTO = convertToDTO(activityRepo.findById(id).get());
		return activityDTO;
	}

	private ActivityDTO convertToDTO(Activity activity) {
		ActivityDTO dto = new ActivityDTO(activity.getTimeStamp(), activity.getPerformed());
		return dto;
	}

	private List<ActivityDTO> convertListToDTO(List<Activity> activityList) {
		ArrayList<ActivityDTO> dtoList = new ArrayList<ActivityDTO>();
		for (Activity activity : activityList) {
			dtoList.add(new ActivityDTO(activity.getTimeStamp(), activity.getPerformed()));
		}
		return dtoList;
	}

	private Activity convertToActivity(ActivityDTO dto) {
		Activity activity = new Activity();
		activity.setPerformed(dto.isPerformed());
		activity.setTimeStamp(dto.getTimeStamp());
		return activity;
	}

	@Override
	protected Activity getActivityByID(Long id) {
		return activityRepo.getOne(id);
	}

}
