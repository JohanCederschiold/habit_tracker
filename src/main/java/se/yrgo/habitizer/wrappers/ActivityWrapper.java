package se.yrgo.habitizer.wrappers;

import java.sql.Date;
import java.time.LocalTime;

public class ActivityWrapper {
	
	public ActivityWrapper(){}
	
	private Date fromDate;
	private LocalTime fromTime;
	private Date toDate;
	private LocalTime toTime;
	private Long id;
	
	public Long getId() {
		return id;
	}
	
	public Date getFromDate() {
		return this.fromDate;
	}
	
	public LocalTime getFromTime() {
		return this.fromTime;
	}
	
	public Date getToDate() {
		return this.toDate;
	}
	
	public LocalTime getToTime() {
		return this.toTime;
	}

}
